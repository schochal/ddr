<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminReportsController extends AbstractController
{
    #[Route('/admin/reports', name: 'admin_reports')]
    public function index(): Response
    {
				if($this->getUser() == null) return new RedirectResponse('/login');
        return $this->render('admin_report/index.html.twig');
    }
}
