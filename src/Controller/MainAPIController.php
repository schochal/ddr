<?php

namespace App\Controller;

use App\Entity\Post;
use App\Repository\PostRepository;

use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class MainAPIController extends AbstractController
{
    #[Route('/main-api-insert', name: 'main_api_insert')]
    public function index(Request $request, ManagerRegistry $doctrine, PostRepository $pr): Response
    {
        $data = $request->getContent();
        $data = json_decode($data);

        $user = $this->getUser();
        $team = $user->getTeam();

        $post = $pr->findOneByTeam($team);
        if($post == null) {

          $timestamp = new \DateTimeImmutable();
          $timestamp_str = $timestamp->format('Y-m-d_H-i-s');
          foreach(['lin', 'exp'] as $file) {
            $exp = explode('.', $data->$file);
            $ext = $exp[count($exp)-1];
            $filename = $file . '_' . $timestamp_str . '.' . $ext;
            rename('media/' . $data->$file, 'media/' . $filename); 
            if($ext == 'pdf') {
              $filename_png = $file . '_' . $timestamp_str . '.png';
              exec('convert -density 300 "media/' . $filename . '" "media/' . $filename_png . '"');
              $ext = 'png';
            }
          }

          $post = new Post();
          $post->setTeam($team);
          $post->setTimestamp($timestamp_str);
          
        } else {
          $timestamp_str = $post->getTimestamp();
          foreach(['lin', 'exp'] as $file) {
            $exp = explode('.', $data->$file);
            $ext = $exp[count($exp)-1];
            $filename = $file . '_' . $timestamp_str . '.' . $ext;
            rename('media/' . $data->$file, 'media/' . $filename); 
            if($ext == 'pdf') {
              $filename_png = $file . '_' . $timestamp_str . '.png';
              exec('convert -density 300 "media/' . $filename . '" "media/' . $filename_png . '"');
              $ext = 'png';
            }
          }
        }

        $post->setDvH1($data->DvH);
        $post->setDvH1_ci($data->DvH_ci);
        $post->setTb1($data->Tb);
        $post->setTb1_ci($data->Tb_ci);
        $post->setDvS1($data->DvS);
        $post->setDvS1_ci($data->DvS_ci);
        $post->setDvH2($data->DvH2);
        $post->setDvH2_ci($data->DvH2_ci);
        $post->setTb2($data->Tb2);
        $post->setTb2_ci($data->Tb2_ci);
        $post->setDvS2($data->DvS2);
        $post->setDvS2_ci($data->DvS2_ci);
        $post->setImage($timestamp_str . '.' . $ext);

        $em = $doctrine->getManager();
        $em->persist($post);
        $em->flush();

        return new Response($timestamp_str);
    }

    #[Route('/main-api-image', name: 'main_api_image')]
    public function blog_api_image(Request $request): Response
    {
        $lin = $request->files->get('lin');
        $exp = $request->files->get('exp');

        $lin->move(
          'media',
          $lin->getClientOriginalName()
        );
        $exp->move(
          'media',
          $exp->getClientOriginalName()
        );
        
        return new Response('OK');
    }

    #[Route('/main-api', name: 'main_api')]
    public function blog_api(PostRepository $pr): JsonResponse
    {
        $data = $pr->findAll();

        $formatted_data = [];

        foreach($data as $entry) {
          $timestamp = \DateTimeImmutable::createFromFormat('Y-m-d_H-i-s', $entry->getTimestamp())->setTimezone(new \DateTimeZone('CET'));
          $team = $entry->getTeam()->setGroupNr(null);
          $team = $entry->getTeam()->removeReports(null);
          $compounds = $team->getCompounds();
          
          $compounds_extracted = explode(' / ', $compounds);

          array_push(
            $formatted_data,
            [
              'compound1' => substr($compounds_extracted[0], 0, -4),
              'compound2' => substr($compounds_extracted[1], 0, -4),
              'team' => $team,
              'timestamp' => $timestamp->format('F jS, Y H:i:s'),
              'image' => $entry->getImage(),
              'DvH' => $entry->getDvH1(),
              'DvH_ci' => $entry->getDvH1_ci(),
              'Tb' => $entry->getTb1(),
              'Tb_ci' => $entry->getTb1_ci(),
              'DvS' => $entry->getDvS1(),
              'DvS_ci' => $entry->getDvS1_ci(),
              'DvH2' => $entry->getDvH2(),
              'DvH2_ci' => $entry->getDvH2_ci(),
              'Tb2' => $entry->getTb2(),
              'Tb2_ci' => $entry->getTb2_ci(),
              'DvS2' => $entry->getDvS2(),
              'DvS2_ci' => $entry->getDvS2_ci(),
            ]
          );
        }

        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        $json = $serializer->normalize($formatted_data, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);

        return new JsonResponse($json);
    }
}
