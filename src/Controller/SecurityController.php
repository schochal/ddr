<?php

namespace App\Controller;
use App\Entity\User;
use App\Repository\UserRepository;

use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Http\Attribute\CurrentUser;

class SecurityController extends AbstractController
{
    #[Route(path: '/login', name: 'app_login')]
    public function login(Request $request, AuthenticationUtils $authenticationUtils): Response
    {
        return $this->render('security/login.html.twig');
    }

    #[Route('/login-api', name: 'login-api')]
    public function index(Request $request): Response
    {
        return $this->json([
          'status' => 'OK',
          'message' => 'successfully logged in',
        ]);
    }

    #[Route('/change-password-api', name: 'change-password-api')]
    public function change_password(Request $request, UserRepository $ur, UserPasswordHasherInterface $pwhi, ManagerRegistry $doctrine)
    {
        $data = $request->getContent();
        $data = json_decode($data);
        
        $em = $doctrine->getManager();
        $user = $this->getUser();
        $hashedPassword = $pwhi->hashPassword(
            $user,
            $data->password
        );
        $user->setPassword($hashedPassword);
        $em->persist($user);
        $em->flush();
        return new Response('OK');
    }

    #[Route(path: '/logout', name: 'app_logout')]
    public function logout(): void
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }
}
