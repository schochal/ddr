<?php

namespace App\Controller;

use App\Entity\Group;
use App\Entity\Team;
use App\Entity\User;
use App\Repository\GroupRepository;
use App\Repository\TeamRepository;
use App\Repository\UserRepository;

use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class AdminAPIController extends AbstractController
{
    #[Route('/admin/admin-api-students', name: 'admin_api_students')]
    public function index(Request $request, ManagerRegistry $doctrine, GroupRepository $gr, TeamRepository $tr, UserRepository $ur, UserPasswordHasherInterface $pwhi, MailerInterface $mailer): Response
    {
        $file = $request->files->get('file');
        $file->move(
          'csv',
          $file->getClientOriginalName()
        );

        $contents = file('csv/' . $file->getClientOriginalName());

        $em = $doctrine->getManager();

        $groups = $gr->findAll();

        foreach($contents as $line) {
          $fields = explode(',', $line);

          $nethz = $fields[0];
          $fn = $fields[1];
          $ln = $fields[2];
          $group = $fields[3];
          $team = $fields[4];
          $sex = $fields[5];
          $compounds = $fields[6];

          $existingGroup = $gr->findOneByNr($group);
          
          //$existingGroup = $gr->findOneById($group);
          if($existingGroup == null) {
            $newGroup = new Group();
            $newGroup->setGroupNr($group);
            $em->persist($newGroup);
            $em->flush();
            $existingGroup = $gr->findOneByNr($group);
          }

          $existingTeam = $tr->findOneByIdAndGroup($team, $existingGroup);
          if($existingTeam == null) {
            $newTeam = new Team();
            $newTeam->setTeamNr($team);
            $newTeam->setGroupNr($existingGroup);
            $newTeam->setCompounds($compounds);
            $em->persist($newTeam);
            $em->flush();
            $existingTeam = $tr->findOneByIdAndGroup($team, $existingGroup);
          }

          $existingUser = $ur->findOneByNethz($nethz);
          if($existingUser == null) {
            $password = $this->generatePassword(12);
            $newUser = new User();
            $newUser->setUsername($nethz);
            $newUser->setFirstName($fn);
            $newUser->setLastName($ln);
            $newUser->setRoles(["ROLE_USER"]);
            $newUser->setIsMale($sex == 'm' ? true : false);
            $newUser->setTeam($existingTeam);

            $hashedPassword = $pwhi->hashPassword(
                $newUser,
                $password
            );

            $newUser->setPassword($hashedPassword);
            $em->persist($newUser);
            $em->flush();
            
            $this->sendPassword($password, $nethz, $sex == 'm' ? 'geschätzter' : 'geschätzte', $fn, $mailer);
          }
        }

        return new Response('OK');
    }

    private function generatePassword(int $length): string {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
      
        for ($i = 0; $i < $length; $i++) {
            $index = rand(0, strlen($characters) - 1);
            $randomString .= $characters[$index];
        }
      
        return $randomString;
    }

    private function sendPassword(string $password, string $nethz, string $opening, string $name, MailerInterface $mailer) {
        $address = $nethz . '@student.ethz.ch';
        if($nethz == "ERME") { 
            $address = "erich.meister@phys.chem.ethz.ch";
        } else if ($nethz == "jona.bredehoeft") {
            $address = "jona.bredehoeft@phys.chem.ethz.ch";
        } else if ($nethz == "raffaele.lanini" ) {
            $address = "raffaele.lanini@usys.ethz.ch";
        }
        //$address = 'schochal@student.ethz.ch';
        $email = (new Email())
            ->from('schochal@student.ethz.ch')
            ->to($address)
            ->subject('[DDR] Login für DDR-Applikation')
            ->html('
                <p>' . $opening . ' ' . $name . ',</p>
                <p>Bald wirst du deinen Versuch "Dampfdruck" starten. Dazu verwenden wir eine <a href="https://ddr.aschoch.ch">Webapplikation</a> [1] für Berichte, Auswertung und Resultate. Deine Login-Informationen:</p>
                <ul>
                  <li>Benutzername: <b>' . $nethz . '</b></li>
                  <li>Passwort: <b>' . $password . '</b></li>
                </ul>
                <p>Da dieser Account lediglich für den DDR-Versuch benutzt wird und am Ende des Semesters wieder gelöscht wird, musst du das Passwort nicht zwingend ändern und kannst es einfach im Browser speichern.</p>
                <p>Freundliche Grüsse und bis bald,<br>Jona Bredehöft / David Schauer / Alexander Schoch</p>
                <p>[1] <a href="https://ddr.aschoch.ch">https://ddr.aschoch.ch</a></p>
                <p><i>Diese E-Mail wurde automatisch generiert</i></p>
            ');

        $mailer->send($email);
        return;
    }

    #[Route('/admin/admin-api', name: 'admin_api')]
    public function blog_api(GroupRepository $gr): JsonResponse
    {
        $data = $gr->findAll();

        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        foreach($data as $group) {
          foreach($group->getTeams() as $team) {
            foreach($team->getReports() as $report) {
              $timestamp = \DateTimeImmutable::createFromFormat('Y-m-d_H-i-s', $report->getTimestamp())->setTimezone(new \DateTimeZone('CET'));
              $report->setTimestamp($timestamp->format('F jS Y, H:i:s'));
            }
          }
        }

        $json = $serializer->normalize($data, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);

        return new JsonResponse($json);
    }
}
