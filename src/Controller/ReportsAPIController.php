<?php

namespace App\Controller;

use App\Entity\Report;
use App\Entity\Team;
use App\Repository\ReportRepository;
use App\Repository\TeamRepository;

use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ReportsAPIController extends AbstractController
{
    #[Route('/reports/reports-api-insert', name: 'reports_api_insert')]
    public function index(Request $request, ManagerRegistry $doctrine, TeamRepository $tr, MailerInterface $mailer): Response
    {
        $data = $request->getContent();
        $data = json_decode($data);

        $user = $this->getUser();
        $team = $user->getTeam();

        $isByAdmin = false; 
        $mark = null;
        $isAccepted = false;
        if(in_array("ROLE_ADMIN", $user->getRoles())) {
          $isByAdmin = true; 
          $mark = $data->mark;
          $isAccepted = $data->isAccepted;
          $team = $tr->findOneById($data->teamId);
        }

        $timestamp = new \DateTimeImmutable();
        $timestamp_str = $timestamp->format('Y-m-d_H-i-s');
        $exp = explode('.', $data->file);
        $ext = $exp[count($exp)-1];
        $filename = $team->getId() . '_' . $timestamp_str . '.' . $ext;
        rename('reports/' . $data->file, 'reports/' . $filename); 

        $report = new Report();
        $report->setGroupNr($team);
        $report->setComment($data->comment);
        $report->setFilename($filename);
        $report->setTimestamp($timestamp_str);
        $report->setIsByAdmin($isByAdmin);
        $report->setIsAccepted($isAccepted);
        $report->setMark($mark);

        $em = $doctrine->getManager();
        $em->persist($report);
        $em->flush();

        if($isByAdmin) {
            $this->sendmail($team, $mailer);
        }
        return new Response('OK');
    }

    #[Route('/reports/reports-api-file', name: 'reports_api_file')]
    public function reports_api_file(Request $request): Response
    {
        $file = $request->files->get('file');

        $file->move(
          'reports',
          $file->getClientOriginalName()
        );
        
        return new Response('OK');
    }

    #[Route('/reports/reports-api-data', name: 'reports_api_data')]
    public function reports_api_data(Request $request): JsonResponse
    {
        $files = scandir('../public/TREVAC/');

        $user = $this->getUser();
        $team = $user->getTeam();
        $group = $team->getGroupNr();

        $filteredFiles = array();

        foreach($files as $file) {
          if(str_contains($file, 'TREVAC_' . $group->getGroupNr() . '_' . $team->getTeamNr())) {
            array_push(
              $filteredFiles,
              $file
            );
          }
        }

        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        $json = $serializer->normalize($filteredFiles, 'json');

        return new JsonResponse($json);
    }

    private function sendmail(Team $team, MailerInterface $mailer): void
    {
        $users = $team->getUsers();
        foreach($users as $user) {
          $nethz = $user->getUsername();
          $address = $nethz . '@student.ethz.ch';
          $opening = $user->getIsMale() == true ? 'geschätzter' : 'geschätzte';
          $name = $user->getFirstName();
          $email = (new Email())
              ->from('schochal@student.ethz.ch')
              ->to($address)
              ->subject('[DDR] Berichtkorrektur Verfügbar')
              ->html('
                  <p>' . $opening . ' ' . $name . ',</p>
                  <p>Die Korrektur eures Berichts für den Versuch Dampfdruck ist nun auf <a href="https://ddr.aschoch.ch">https://ddr.aschoch.ch</a> unter "Reports" Verfügbar.</p>
                  <p>Freundliche Grüsse,<br>Jona Bredehöft / David Schauer / Alexander Schoch</p>
                  <p><i>Diese E-Mail wurde automatisch generiert</i></p>
              ');

          $mailer->send($email);
        }
        return;
    }

    #[Route('/reports/reports-api', name: 'reports_api')]
    public function reports_api(ReportRepository $rr): JsonResponse
    {
        $data = $rr->findByTeam($this->getUser()->getTeam()->getId());

        $formatted_data = [];

        foreach($data as $entry) {
          $timestamp = \DateTimeImmutable::createFromFormat('Y-m-d_H-i-s', $entry->getTimestamp())->setTimezone(new \DateTimeZone('CET'));

          array_push(
            $formatted_data,
            [
              'comment' => $entry->getComment(),
              'isAccepted' => $entry->getIsAccepted(),
              'isByAdmin' => $entry->getIsByAdmin(),
              'mark' => $entry->getMark(),
              'timestamp' => $timestamp->format('F jS Y, H:i:s'),
              'filename' => $entry->getFilename(),
            ]
          );
        }

        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        $json = $serializer->normalize($formatted_data, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);

        return new JsonResponse($json);
    }
}
