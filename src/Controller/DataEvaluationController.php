<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DataEvaluationController extends AbstractController
{
    #[Route('/data_evaluation', name: 'data_evaluation')]
    public function index(): Response
    {
        return $this->render('data_evaluation/index.html.twig');
    }
}
