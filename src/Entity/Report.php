<?php

namespace App\Entity;

use App\Repository\ReportRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ReportRepository::class)]
class Report
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $timestamp;

    #[ORM\ManyToOne(targetEntity: Team::class, inversedBy: 'reports')]
    #[ORM\JoinColumn(nullable: false)]
    private $groupNr;

    #[ORM\Column(type: 'string', length: 255)]
    private $filename;

    #[ORM\Column(type: 'boolean')]
    private $isByAdmin;

    #[ORM\Column(type: 'boolean')]
    private $isAccepted;

    #[ORM\Column(type: 'float', nullable: true)]
    private $mark;

    #[ORM\Column(type: 'text', nullable: true)]
    private $comment;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTimestamp(): ?string
    {
        return $this->timestamp;
    }

    public function setTimestamp(string $timestamp): self
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    public function getGroupNr(): ?Team
    {
        return $this->groupNr;
    }

    public function setGroupNr(?Team $groupNr): self
    {
        $this->groupNr = $groupNr;

        return $this;
    }

    public function getFilename(): ?string
    {
        return $this->filename;
    }

    public function setFilename(string $filename): self
    {
        $this->filename = $filename;

        return $this;
    }

    public function getIsByAdmin(): ?bool
    {
        return $this->isByAdmin;
    }

    public function setIsByAdmin(bool $isByAdmin): self
    {
        $this->isByAdmin = $isByAdmin;

        return $this;
    }

    public function getIsAccepted(): ?bool
    {
        return $this->isAccepted;
    }

    public function setIsAccepted(bool $isAccepted): self
    {
        $this->isAccepted = $isAccepted;

        return $this;
    }

    public function getMark(): ?float
    {
        return $this->mark;
    }

    public function setMark(?float $mark): self
    {
        $this->mark = $mark;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }
}
