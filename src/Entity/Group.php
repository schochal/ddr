<?php

namespace App\Entity;

use App\Repository\GroupRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: GroupRepository::class)]
#[ORM\Table(name: '`group`')]
class Group
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\OneToMany(mappedBy: 'groupNr', targetEntity: Team::class)]
    #[ORM\OrderBy(["teamNr" => "ASC"])]
    private $teams;

    #[ORM\Column(type: 'string')]
    private $GroupNr;

    public function __construct()
    {
        $this->teams = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection<int, Team>
     */
    public function getTeams(): Collection
    {
        return $this->teams;
    }

    public function addTeam(Team $team): self
    {
        if (!$this->teams->contains($team)) {
            $this->teams[] = $team;
            $team->setGroupNr($this);
        }

        return $this;
    }
		
		public function removeTeams(): self
		{
				foreach($this->teams as $team) {
						$this->teams->removeelement($team);
				}
				return $this;
		}

    public function removeTeam(Team $team): self
    {
        if ($this->teams->removeElement($team)) {
            // set the owning side to null (unless already changed)
            if ($team->getGroupNr() === $this) {
                $team->setGroupNr(null);
            }
        }

        return $this;
    }

    public function getGroupNr(): ?string
    {
        return $this->GroupNr;
    }

    public function setGroupNr(string $GroupNr): self
    {
        $this->GroupNr = $GroupNr;

        return $this;
    }
}
