<?php

namespace App\Entity;

use App\Repository\TeamRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TeamRepository::class)]
class Team
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\OneToMany(mappedBy: 'groupNr', targetEntity: Report::class, orphanRemoval: true)]
    #[ORM\OrderBy(["timestamp" => "DESC"])]
    private $reports;

    #[ORM\OneToMany(mappedBy: 'team', targetEntity: User::class)]
    private $users;

    #[ORM\ManyToOne(targetEntity: Group::class, inversedBy: 'teams')]
    #[ApiProperty(readableLink: false, writableLink: false)]
    private $groupNr;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $teamNr;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $compounds;

    public function __construct()
    {
        $this->reports = new ArrayCollection();
        $this->users = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGroupNr(): ?Group
    {
        return $this->groupNr;
    }

    public function setGroupNr(?Group $groupNr): self
    {
        $this->groupNr = $groupNr;

        return $this;
    }

    /**
     * @return Collection<int, Report>
     */
    public function getReports(): Collection
    {
        return $this->reports;
    }

    public function addReport(Report $report): self
    {
        if (!$this->reports->contains($report)) {
            $this->reports[] = $report;
            $report->setGroupNr($this);
        }

        return $this;
    }

		public function removeReports(): self
		{
				foreach($this->reports as $report) {
						$this->reports->removeElement($report);
				}
				return $this;
		}

    public function removeReport(Report $report): self
    {
        if ($this->reports->removeElement($report)) {
            // set the owning side to null (unless already changed)
            if ($report->getGroupNr() === $this) {
                $report->setGroupNr(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setTeam($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getTeam() === $this) {
                $user->setTeam(null);
            }
        }

        return $this;
    }

    public function getTeamNr(): ?int
    {
        return $this->teamNr;
    }

    public function setTeamNr(?int $teamNr): self
    {
        $this->teamNr = $teamNr;

        return $this;
    }

    public function getCompounds(): ?string
    {
        return $this->compounds;
    }

    public function setCompounds(?string $compounds): self
    {
        $this->compounds = $compounds;

        return $this;
    }
}
