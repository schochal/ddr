<?php

namespace App\Entity;

use App\Repository\PostRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PostRepository::class)]
class Post
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $timestamp;

    #[ORM\Column(type: 'string', length: 255)]
    private $image;

    #[ORM\ManyToOne(targetEntity: Team::class)]
    #[ORM\JoinColumn(nullable: true)]
    private $team;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $DvH1;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $DvH1_ci;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $DvS1;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $DvS1_ci;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $Tb1;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $Tb1_ci;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $DvH2;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $DvH2_ci;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $Tb2;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $Tb2_ci;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $DvS2;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $DvS2_ci;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTimestamp(): ?string
    {
        return $this->timestamp;
    }

    public function setTimestamp(string $timestamp): self
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getTeam(): ?Team
    {
        return $this->team;
    }

    public function setTeam(?Team $team): self
    {
        $this->team = $team;

        return $this;
    }

    public function getDvH1(): ?string
    {
        return $this->DvH1;
    }

    public function setDvH1(?string $DvH1): self
    {
        $this->DvH1 = $DvH1;

        return $this;
    }

    public function getDvH1_ci(): ?string
    {
        return $this->DvH1_ci;
    }

    public function setDvH1_ci(?string $DvH1_ci): self
    {
        $this->DvH1_ci = $DvH1_ci;

        return $this;
    }

    public function getTb1(): ?string
    {
        return $this->Tb1;
    }

    public function setTb1(?string $Tb1): self
    {
        $this->Tb1 = $Tb1;

        return $this;
    }

    public function getTb1_ci(): ?string
    {
        return $this->Tb1_ci;
    }

    public function setTb1_ci(?string $Tb1_ci): self
    {
        $this->Tb1_ci = $Tb1_ci;

        return $this;
    }

    public function getDvS1(): ?string
    {
        return $this->DvS1;
    }

    public function setDvS1(?string $DvS1): self
    {
        $this->DvS1 = $DvS1;

        return $this;
    }

    public function getDvS1_ci(): ?string
    {
        return $this->DvS1_ci;
    }

    public function setDvS1_ci(?string $DvS1_ci): self
    {
        $this->DvS1_ci = $DvS1_ci;

        return $this;
    }

    public function getDvH2(): ?string
    {
        return $this->DvH2;
    }

    public function setDvH2(?string $DvH2): self
    {
        $this->DvH2 = $DvH2;

        return $this;
    }

    public function getDvH2_ci(): ?string
    {
        return $this->DvH2_ci;
    }

    public function setDvH2_ci(?string $DvH2_ci): self
    {
        $this->DvH2_ci = $DvH2_ci;

        return $this;
    }

    public function getTb2(): ?string
    {
        return $this->Tb2;
    }

    public function setTb2(?string $Tb2): self
    {
        $this->Tb2 = $Tb2;

        return $this;
    }

    public function getTb2_ci(): ?string
    {
        return $this->Tb2_ci;
    }

    public function setTb2_ci(?string $Tb2_ci): self
    {
        $this->Tb2_ci = $Tb2_ci;

        return $this;
    }

    public function getDvS2(): ?string
    {
        return $this->DvS2;
    }

    public function setDvS2(?string $DvS2): self
    {
        $this->DvS2 = $DvS2;

        return $this;
    }

    public function getDvS2_ci(): ?string
    {
        return $this->DvS2_ci;
    }

    public function setDvS2_ci(?string $DvS2_ci): self
    {
        $this->DvS2_ci = $DvS2_ci;

        return $this;
    }
}
