import React from 'react';
import ReactDOM from 'react-dom';

import {
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Alert,
  Autocomplete,
  Button,
  Grid,
  Snackbar,
  TextField,
  Typography,
} from '@mui/material';

import LoadingButton from '@mui/lab/LoadingButton';

import { ThemeProvider } from '@mui/material/styles';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import FileUploadIcon from '@mui/icons-material/FileUpload';

import theme from './theme';

import axios from 'axios';

class BlogForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      lin: '',
      exp: '',
			DvH: '',
			DvH_ci: '',
			Tb: '',
			Tb_ci: '',
			DvS: '',
			DvS_ci: '',
			DvH2: '',
			DvH2_ci: '',
			Tb2: '',
			Tb2_ci: '',
			DvS2: '',
			DvS2_ci: '',
			compounds: '',
      snackbar: false,
      snackbarError: false,
			isUploading: false,
			isPdf: false,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.getCompounds = this.getCompounds.bind(this);
  }

	componentDidMount() {
		this.getCompounds();
	}

  handleSubmit(e) {
		e.preventDefault();
    if(this.state.lin != '' && this.state.exp != '' && this.state.DvH != '' && this.state.DvH_ci != '' && this.state.Tb != '' && this.state.Tb_ci != '' && this.state.DvS != '' && this.state.DvS_ci != '' && this.state.DvH2 != '' && this.state.DvH2_ci != '' && this.state.Tb2 != '' && this.state.Tb2_ci != '' && this.state.DvS2 != '' && this.state.DvS2_ci != '') {
			this.setState({isUploading: true});
      let formData = new FormData();
      formData.append('lin', this.state.lin);
      formData.append('exp', this.state.exp);
      axios.post('/main-api-image', formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      }).then((response) => {
        axios.put('/main-api-insert', {
          DvH: this.state.DvH,
          DvH_ci: this.state.DvH_ci,
          Tb: this.state.Tb,
          Tb_ci: this.state.Tb_ci,
					DvS: this.state.DvS,
					DvS_ci: this.state.DvS_ci,
          DvH2: this.state.DvH2,
          DvH2_ci: this.state.DvH2_ci,
          Tb2: this.state.Tb2,
          Tb2_ci: this.state.Tb2_ci,
					DvS2: this.state.DvS2,
					DvS2_ci: this.state.DvS2_ci,
          lin: this.state.lin.name,
          exp: this.state.exp.name,
        }).then(response => {
          this.setState({
            lin: '',
            exp: '',
            DvH: '',
            DvH_ci: '',
            Tb: '',
            Tb_ci: '',
						DvS: '',
						DvS_ci: '',
            DvH2: '',
            DvH2_ci: '',
            Tb2: '',
            Tb2_ci: '',
						DvS2: '',
						DvS2_ci: '',
            snackbar: true,
            snackbarError: false,
						isUploading: false,
						isPdf: false,
          });
					this.props.getData();
        });
      });
    } else {
      this.setState({snackbarError: true});
    }
  }

	async getCompounds() {
    const response = await fetch('/user-api');
    const data = await response.json();
		this.setState({
			compounds: data.team.compounds,
		});
	}

  render() {
    return (
      <ThemeProvider theme={theme}>
        <Accordion style={{ marginTop: '20px' }} elevation={6}>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon/>}
            aria-controls="panel1a-content"
          >
            <Typography variant="h5">
              {this.props.hasAPost ? "Update Post" : "Create Post"}
            </Typography>
          </AccordionSummary>
          <AccordionDetails>
						{this.state.compounds != '' &&
						<form>
							<Grid container spacing={2}>
								<Grid item xs={12}>
									<Typography variant="h6">
										{this.state.compounds.split('(')[0].slice(0,-1)}
									</Typography>
								</Grid>
								<Grid item xs={12} md={6}>
									<TextField 
										fullWidth
										label={<>Δ<sub>v</sub><i>H</i>&nbsp;[kJ mol<sup>-1</sup>]</>}
										variant="outlined" 
										value={this.state.DvH}
										onChange={(e) => this.setState({DvH: e.target.value})}
									/>
								</Grid>
								<Grid item xs={12} md={6}>
									<TextField 
										fullWidth
										label={<>±</>}
										variant="outlined" 
										value={this.state.DvH_ci}
										onChange={(e) => this.setState({DvH_ci: e.target.value})}
									/>
								</Grid>
								<Grid item xs={12} md={6}>
									<TextField 
										fullWidth
										label={<><i>T</i><sub>b</sub> [°C]</>}
										variant="outlined" 
										value={this.state.Tb}
										onChange={(e) => this.setState({Tb: e.target.value})}
									/>
								</Grid>
								<Grid item xs={12} md={6}>
									<TextField 
										fullWidth
										label={<>±</>}
										variant="outlined" 
										value={this.state.Tb_ci}
										onChange={(e) => this.setState({Tb_ci: e.target.value})}
									/>
								</Grid>
								<Grid item xs={12} md={6}>
									<TextField 
										fullWidth
										label={<>Δ<sub>v</sub><i>S</i> [J mol<sup>-1</sup> K<sup>-1</sup>]</>}
										variant="outlined" 
										value={this.state.DvS}
										onChange={(e) => this.setState({DvS: e.target.value})}
									/>
								</Grid>
								<Grid item xs={12} md={6}>
									<TextField 
										fullWidth
										label={<>±</>}
										variant="outlined" 
										value={this.state.DvS_ci}
										onChange={(e) => this.setState({DvS_ci: e.target.value})}
									/>
								</Grid>

								<Grid item xs={12}>
									<Typography variant="h6">
										{this.state.compounds.split('(')[1].slice(5,-1)}
									</Typography>
								</Grid>
								<Grid item xs={12} md={6}>
									<TextField 
										fullWidth
										label={<>Δ<sub>v</sub><i>H</i>&nbsp;[kJ mol<sup>-1</sup>]</>}
										variant="outlined" 
										value={this.state.DvH2}
										onChange={(e) => this.setState({DvH2: e.target.value})}
									/>
								</Grid>
								<Grid item xs={12} md={6}>
									<TextField 
										fullWidth
										label={<>±</>}
										variant="outlined" 
										value={this.state.DvH2_ci}
										onChange={(e) => this.setState({DvH2_ci: e.target.value})}
									/>
								</Grid>
								<Grid item xs={12} md={6}>
									<TextField 
										fullWidth
										label={<><i>T</i><sub>b</sub> [°C]</>}
										variant="outlined" 
										value={this.state.Tb2}
										onChange={(e) => this.setState({Tb2: e.target.value})}
									/>
								</Grid>
								<Grid item xs={12} md={6}>
									<TextField 
										fullWidth
										label={<>±</>}
										variant="outlined" 
										value={this.state.Tb2_ci}
										onChange={(e) => this.setState({Tb2_ci: e.target.value})}
									/>
								</Grid>
								<Grid item xs={12} md={6}>
									<TextField 
										fullWidth
										label={<>Δ<sub>v</sub><i>S</i>&nbsp;[J mol<sup>-1</sup> K<sup>-1</sup>]</>}
										variant="outlined" 
										value={this.state.DvS2}
										onChange={(e) => this.setState({DvS2: e.target.value})}
									/>
								</Grid>
								<Grid item xs={12} md={6}>
									<TextField 
										fullWidth
										label={<>±</>}
										variant="outlined" 
										value={this.state.DvS2_ci}
										onChange={(e) => this.setState({DvS2_ci: e.target.value})}
									/>
								</Grid>
	
								<Grid item xs={12}>
									<Button
										variant="contained"
										component="label"
										startIcon={<FileUploadIcon/>}
									>
										<input name="upload-photo" type="file" accept="image/png, image/jpeg, application/pdf" hidden onChange={(e) => this.setState({exp: e.target.files[0], isPdf: e.target.files[0].name.split('.').pop() == 'pdf' ? true : this.state.isPdf})}/>
										Upload Exponential Form
									</Button>
									{this.state.exp != '' && 
										<Typography component="span">  Selected: {this.state.exp.name}</Typography>
									}
								</Grid>
								<Grid item xs={12}>
									<Button
										variant="contained"
										component="label"
										startIcon={<FileUploadIcon/>}
									>
										<input name="upload-photo" type="file" accept="image/png, image/jpeg, application/pdf" hidden onChange={(e) => this.setState({lin: e.target.files[0], isPdf: e.target.files[0].name.split('.').pop() == 'pdf' ? true : this.state.isPdf})}/>
										Upload Linear Form
									</Button>
									{this.state.lin != '' && 
										<Typography component="span">  Selected: {this.state.lin.name}</Typography>
									}
								</Grid>

								{this.state.isPdf &&
									<Grid item xs={12}>
										<Alert severity="info">PDF files have to be converted to images first and take thus longer to process. Please be patient.</Alert>
									</Grid>
								}
	
								<Grid item xs={12}>
									{this.state.isUploading ? 
									<LoadingButton
										variant="contained"
										loading
									>
										Submit
									</LoadingButton> :
									<Button
										type="submit"
										variant="contained"
										onClick={(e) => this.handleSubmit(e)}
									>
										Submit
									</Button>}
								</Grid>
							</Grid>
						</form>}
          </AccordionDetails>
        </Accordion>
        <Snackbar open={this.state.snackbar} autoHideDuration={6000} onClose={() => {this.setState({snackbar: false});}}>
          <Alert severity="success" sx={{ width: '100%' }}>
           The Post was successfully added! 
          </Alert>
        </Snackbar>
        <Snackbar open={this.state.snackbarError} autoHideDuration={6000} onClose={() => {this.setState({snackbarError: false});}}>
          <Alert severity="error" sx={{ width: '100%' }}>
           Please fill out the form completely
          </Alert>
        </Snackbar>
      </ThemeProvider>
    )
  }
}

export default BlogForm;
