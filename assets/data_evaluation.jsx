import React from 'react';
import ReactDOM from 'react-dom';

import { MathJax, MathJaxContext } from "better-react-mathjax";

import {
  Alert,
  Box,
  Container,
  Paper,
  Typography
} from '@mui/material';

import {ThemeProvider} from '@mui/material/styles';
import theme from './theme';

class DataEvaluation extends React.Component {
  constructor(props) {
    super(props);

    this.image = this.image.bind(this);
  }

  image(file) {
    return (
      <Paper elevation={1} style={{ width: window.innerWidth > 1000 ? '50%' : '100%', borderRadius: '10px', marginLeft: 'auto', marginRight: 'auto', marginTop: '20px', marginBottom: '20px' }}>
        <img src={file} style={{ width: '100%', borderRadius: '10px' }}/>
      </Paper>
    );
}

  render() {
    return (
      <ThemeProvider theme={theme}>
        <Container maxWidth="xl" style={{ marginTop: '20px' }}>
					<MathJaxContext>
						<Typography variant="h4" style={{ marginTop: '20px', marginBottom: '20px' }}>
							Data Evaluation
						</Typography>
						<Typography>
							On this page, you'll find a short guide on how to evaluate your measured data and code the plots for your report.
						</Typography>

						<Typography variant="h5" style={{ marginTop: '20px', marginBottom: '20px' }}>
							Vapour Pressure Curve
						</Typography>

						<Typography>
							First, read your measured data (<i>θ</i>, <i>P</i><sup>*</sup>) for both compounds into arrays in R. Now, we remember that the Clausius-Clapeyron equation looks like this:
						</Typography>

						<MathJax>{"\\[ P^*(T) = P_0 \\exp \\left\\{ \\frac{\\Delta_\\text{v}H}{R}\\left(\\frac{1}{T_0} - \\frac{1}{T} \\right)\\right\\} \\]"}</MathJax>

						<Typography>
							If we now divide by <i>P</i><sub>0</sub> and take the natural logarithm on both sides, we get
						</Typography>

						<MathJax>{"\\[\\ln \\frac{P^*(T)}{P_0} = \\frac{\\Delta_\\text{v}H}{R}\\left(\\frac{1}{T_0} - \\frac{1}{T} \\right)\\]"}.</MathJax>

						<Typography>
							If we now substitute the left-hand side by <i>y</i> and 1/<i>T</i> by <i>x</i>, it is more easily visible that this is now a linear function, as all the other parameters are constant.
						</Typography>

						<MathJax>{"\\[y = \\frac{\\Delta_\\text{v}H}{R}\\left(\\frac{1}{T_0} - x \\right)\\]"}.</MathJax>

						<Typography>
							This means that if we plot 1/<i>T</i> (<i>x</i>) <i>versus</i> ln<i>P</i>/<i>P</i><sub>0</sub> (<i>y</i>), our data should look linear.
						</Typography>

						{this.image('data_evaluation/lin_points.png')}

						<Typography>
							Now, we fit a linear model to this data using the <code>lm()</code> function. Via <code>summary(model)$coef[1,1]</code> and <code>summary(model)$coef[2,1]</code>, the <i>y</i> intercept and the slope, respecitvely, can be pulled out of the model. From 
						</Typography>

						<MathJax>{"\\[\\text{slope} = -\\frac{\\Delta_\\text{v}H}{R} \\qquad \\Delta_\\text{v}H = - \\text{slope} \\cdot R\\]"}</MathJax>

						<Typography>
							and
						</Typography>

						<MathJax>{"\\[\\text{intercept} = \\frac{\\Delta_\\text{v}H}{RT_\\text{b}} \\qquad T_\\text{b} = \\frac{\\text{slope}}{\\text{intercept}},\\]"}</MathJax>

						<Typography>
							the boiling temperature and the enthalpy of evaporation can be determined. Back to the model: we can fit this model into the <code>abline()</code> function in order to draw a line through the data points. Now, use the <code>legend()</code> function to draw a legend, and the linear version of the vapour pressure diagram is done!
						</Typography>

						{this.image('data_evaluation/lin_fit.png')}

						<Typography>
							For the exponential form, draw the "raw" data points into a new diagram (call <code>plot()</code> again).
						</Typography>

						{this.image('data_evaluation/exp_points.png')}

						<Typography>
							Create two vectors of, say, 200 points in a temperature range you want a line in. Using the boiling temperature and the enthalpy of evaporation from the linear plot before, calculate a <i>P</i><sup>*</sup> vector via the Clausius-Clapeyron equation. Plot this underneath the data points.
						</Typography>

						{this.image('data_evaluation/exp_fit.png')}

						<Typography>
							Now, use the <code>abline()</code> function again to mark the boiling temperature and the atmospheric pressure at sea level and add a legend.
						</Typography>

						{this.image('data_evaluation/exp_finished.png')}

						<Typography>
							Lastly, you can calculate the entrophy of evaporation via
						</Typography>

						<MathJax>{"\\[\\Delta_\\text{v}S = \\frac{\\Delta_\\text{v}H}{T_\\text{b}}.\\]"}</MathJax>

						<Typography>
						Congrats, you are now ready to upload your plots to the <a href="https://ddr.aschoch.ch">Main Page</a> :).
						</Typography>

						<Typography variant="h5" style={{ marginTop: '20px', marginBottom: '20px' }}>
							TREVAC
						</Typography>

						<Typography>
							First, load your data into R. Plot the time <i>versus</i> the temperature.
						</Typography>

						{this.image('data_evaluation/TREVAC_raw.png')}

						<Typography>
							In order to draw the integrals, you can use the <code>polygon()</code> function by passing a subset of the original dataset as an argument. It is a good idea to save those subset in separate variables, as we'll use them after.
						</Typography>

						{this.image('data_evaluation/TREVAC_integrals.png')}

						<Typography>
							For the integration of those areas, we will have to use a numerical integration method, as our function is not continous. We will use the trapezoidal quadrature here, as illustrated below. The <i>rectangular quadrature</i>, we split the integral into many rectangles. The area of each rectangle is calculated by multiplying the function value with the step size <i>h</i>.
						</Typography>

						{this.image('data_evaluation/rectangular_quadrature.png')}

						<Typography>
							The <i>trapezoidal quadrature</i> is very similar, but we approximate the integral by trapezoids. For this area, the step size <i>h</i> is multiplied with the average height of the trapezoid, which is the average of the left and right heights.
						</Typography>

						{this.image('data_evaluation/trapezoidal_quadrature.png')}

						<Typography>
							Thus, we get
						</Typography>

						<MathJax>{"\\[\\int_a^b f(x)\\,\\text{d}x \\approx \\sum_{i = 0}^{N-1} h\\frac{f(a + hi) + f(a + hi+h)}{2} \\qquad h = \\frac{b - a}{N}\\]"}</MathJax>

						<Typography>
							This area is now averaged over all three peaks and a standard deviation is calculated (you can use the <code>mean()</code> and <code>sd()</code> functions for this). By comparing these results with the ones from methanol, an enthalpy of evaporation can be calculated.
						</Typography>

						<Typography>
							Now, draw your integration boundaries into your plot and you're done :).
						</Typography>

						{this.image('data_evaluation/TREVAC_finished.png')}
					</MathJaxContext>
        </Container>
      </ThemeProvider>
    );
  }
}

ReactDOM.render(<DataEvaluation/>, document.getElementById('data_evaluation'));
