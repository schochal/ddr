import React from 'react';
import ReactDOM from 'react-dom';

import {
  Alert,
  Button,
  Checkbox,
  Container,
  FormControlLabel,
  Grid,
  Snackbar,
  TextField,
  Typography,
} from '@mui/material';

import LoadingButton from '@mui/lab/LoadingButton';

import { ThemeProvider } from '@mui/material/styles';

import theme from './theme';

import axios from 'axios';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
			username: '',
			password: '',
			remember_me: false,
      snackbar: false,
      snackbarError: false,
			invalidCredentials: false,
			isLoading: false,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(e) {
		e.preventDefault();
    if(this.state.username != '' && this.state.password != '') {
			this.setState({isLoading: true});
			axios.post('/login-api', {
				username: this.state.username,
				password: this.state.password,
				remember_me: this.state.remember_me,
			}).then(response => {
				if(response.data.status != 'OK')
					this.setState({invalidCredentials: true, isLoading: false});
				else 
					window.location = '/';
			});
    } else {
      this.setState({snackbarError: true});
    }
  }

  render() {
    return (
      <ThemeProvider theme={theme}>
				<Container maxWidth="xl">
					<Typography variant="h4" style={{ marginTop: '20px' }}>Login</Typography>
						<form>
						<Grid container spacing={2} style={{ marginTop: '20px' }}>
							{this.state.invalidCredentials && 
							<Grid item xs={12}>
								<Alert severity="error">Username or Password incorrect</Alert>
							</Grid> }
							<Grid item xs={12} md={6}>
								<TextField
									label="ETH Username"
									variant="outlined"
									value={this.state.username}
									onChange={(e) => this.setState({username: e.target.value})}
									fullWidth
								/>
							</Grid>
							<Grid item xs={12} md={6}>
								<TextField
									label="Password"
									variant="outlined"
									value={this.state.password}
									type="password"
									onChange={(e) => this.setState({password: e.target.value})}
									fullWidth
								/>
							</Grid>
							<Grid item xs={12}>
								<FormControlLabel control={<Checkbox checked={this.state.remember_me} onChange={() => this.setState({remember_me: !this.state.remember_me})} />} label="Remember Me" />
							</Grid>
							<Grid item xs={12}>
								{this.state.isLoading ?
								<LoadingButton
									variant="contained"
									loading
								>
									Submit
								</LoadingButton> :
								<Button
									type="submit"
									variant="contained"
									onClick={(e) => {this.handleSubmit(e)}}
								>
									Submit
								</Button>}
							</Grid>
						</Grid>
					</form>
				</Container>
			</ThemeProvider>
    )
  }
}

ReactDOM.render(<Login/>, document.getElementById('login'));
