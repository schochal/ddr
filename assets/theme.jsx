import {createTheme, ThemeProvider} from '@mui/material/styles';

const fonts = createTheme({
  typography: {
    fontFamily: [
      'cmu',
    ].join(','),
  },
});

const theme = createTheme({
  typography: {
    fontFamily: [
      'cmu',
    ].join(','),
  },
  palette: {
    type: 'light',
		primary: {
      main: '#1e1e6b',
      light: '#8181e2',
      contrastText: '#dbdbf7',
      dark: '#000041',
    },
    secondary: {
      main: '#b71c1c',
      light: '#f7c870',
      dark: '#560c0c',
    },
  },
});

export default theme;
