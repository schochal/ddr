import React from 'react';
import ReactDOM from 'react-dom';
import Plot from 'react-plotly.js';
const brentMethod = require('brents-method')

import {
	Container,
	FormControl,
	Grid,
	InputLabel,
	MenuItem,
	Select,
	Slider,
	Typography,
} from '@mui/material';

import { ThemeProvider } from '@mui/material/styles';

import theme from './theme';

class ClausiusClapeyron extends React.Component {
	constructor(props) {
		super(props);

		const Tmin = 20;
		const Tmax = 100;
		const N    = 100;

		let T = [];

		const dT = (Tmax - Tmin) / (N - 1);
		for(let i = 0; i < N; i++) {
			T.push(i * dT + Tmin);
		}

		this.state = {
			T: T,
			Tlin: null,
			P: null,
			lnP: null,
			DvH: 42.3,
			Tb: 78.4,
			P0: 1013.25,
			Tmin: Tmin,
			Tmax: Tmax,
			N: N,
		}

		this.evaluate = this.evaluate.bind(this);
		this.rootfinding = this.rootfinding.bind(this);
	}

	componentDidMount() {
		this.evaluate();
	}

	evaluate() {
		let Tlin = []
		let P = []
		let lnP = []

		this.state.T.map(Ti => {
			Tlin.push(1 / (Ti + 273.15));
			let Pi = this.state.P0 * Math.exp(this.state.DvH * 1000 / (8.314 * (this.state.Tb + 273.15))) * Math.exp(-this.state.DvH * 1000 / (8.314 * (Ti + 273.15)));
			P.push(Pi);
			lnP.push(Math.log(Pi / this.state.P0));
		});

		this.setState({
			Tlin: Tlin,
			P: P,
			lnP: lnP
		});
	}

	rootfinding(T, xi, L12, L21) {
    //return this.raoult1(xi, this.antoine(T, this.getAntoine(this.state.C1)), L12, L21) + this.raoult2(1-xi, this.antoine(T, this.getAntoine(this.state.C2)), L12, L21) - 1;
	}

	render() {
		return (
      <ThemeProvider theme={theme}>
				<Container maxWidth="xl">
					<Typography variant="h4" style={{ marginTop: '20px', marginBottom: '20px' }}>Clausius-Clapeyron Equation</Typography>
					<Grid container spacing={2} style={{ marginTop: '10px' }}>
						<Grid item xs={12} md={6}>
							<Typography variant="h5">Δ<sub>v</sub><i>H</i> [kJ mol<sup>-1</sup>]</Typography>
							<Slider
								defaultValue={1}
								value={this.state.DvH}
								onChange={(e) => {this.setState({DvH: e.target.value}); this.evaluate();}}
								min={20}
								step={0.1}
								max={60}
								valueLabelDisplay="on"
								marks={[{value: 42.3, label: 'Ethanol'}, {value: 31.0, label: 'n-Hexane'}]}
							/>
						</Grid>
						<Grid item xs={12} md={6}>
							<Typography variant="h5"><i>θ</i><sub>b</sub> [°C]</Typography>
							<Slider
								defaultValue={1}
								value={this.state.Tb}
								onChange={(e) => {this.setState({Tb: e.target.value}); this.evaluate();}}
								min={20}
								step={0.1}
								max={100}
								valueLabelDisplay="on"
								marks={[{value: 78.4, label: 'Ethanol'}, {value: 68.8, label: 'n-Hexane'}]}
							/>
						</Grid>
						{this.state.Tlin !== null &&
						<Grid item xs={12} md={12}>
							<Plot
								style={{ marginLeft: 0}}
								data={[
									{
										x: this.state.Tlin,
										y: this.state.lnP,
										type: 'scatter',
										mode: 'lines',
										marker: {color: 'black'},
										name: 'Linear',
									},
									{
										x: [1 / (this.state.Tb + 273.15), 1 / (this.state.Tb + 273.15)],
										y: [this.state.lnP[this.state.lnP.length - 1], this.state.lnP[0]],
										type: 'scatter',
										mode: 'lines',
										marker: {color: 'black'},
										line: {dash: 'dot', width: 1},
										name: 'Tb',
									}
								]}
								layout={{title: 'Linearized', xaxis: {title: "1 / <i>T</i> [K<sup>-1</sup>]"}, yaxis: {title: "ln <i>P</i>/<i>P</i><sub>0</sub>"}, legend: {xanchor: 'right'}}}
							/>
						</Grid>}
						{this.state.Tlin !== null &&
						<Grid item xs={12} md={12}>
							<Plot
								style={{ marginLeft: 0}}
								data={[
									{
										x: this.state.T,
										y: this.state.P,
										type: 'scatter',
										mode: 'lines',
										marker: {color: 'black'},
										name: 'Exponential'
									},
									{
										x: [this.state.Tmin, this.state.Tmax],
										y: [this.state.P0, this.state.P0],
										type: 'scatter',
										mode: 'lines',
										marker: {color: 'black'},
										line: {dash: 'dash', width: 1},
										name: 'P0',
									},
									{
										x: [this.state.Tb, this.state.Tb],
										y: [this.state.P[this.state.P.length - 1], this.state.P[0]],
										type: 'scatter',
										mode: 'lines',
										marker: {color: 'black'},
										line: {dash: 'dot', width: 1},
										name: 'Tb',
									}
								]}
								layout={{title: 'Exponential', xaxis: {title: "<i>θ</i> [°C]"}, yaxis: {title: "<i>P</i> [mbar]"}, legend: {xanchor: 'left', x: 0}}}
							/>
						</Grid>}
					</Grid>
				</Container>
			</ThemeProvider>
		);
	}
}

ReactDOM.render(<ClausiusClapeyron/>, document.getElementById('clausius-clapeyron'));
