import React from 'react';
import ReactDOM from 'react-dom';

import {
	Box,
	Container,
	Grid,
	Typography,
} from '@mui/material';

import { ThemeProvider } from '@mui/material/styles';
import theme from './theme';

class Footer extends React.Component {
  constructor(props) {
    super(props);
  }

  render () {
    return (
			<ThemeProvider theme={theme}>
				<Box style={{ backgroundColor: theme.palette.primary.dark, paddingTop: '50px', paddingBottom: '50px', marginTop: '50px', color: 'white' }}>
					<Container maxWidth="xl">
						<Grid container>
							<Grid item xs={12} md={6}>
								<Typography variant="h5">	
									About
								</Typography>	
								<Typography>	
									&#169; Alexander Schoch, 2022<br/>
									<a href="mailto:schochal@student.ethz.ch" style={{ color: 'white' }}>schochal@student.ethz.ch</a><br/>
								 	CH – 8050 Zürich<br/><br/>
									Helping Assistant Dampfdruck<br/>
									Praktikum for Physical Chemistry with Dr. Meister<br/>
								</Typography>	
							</Grid>	
							<Grid item xs={12} md={6}>
								<Typography variant="h5" style={{ textAlign: window.innerWidth > 1000 ? 'right' : 'left', marginTop: window.innerWidth > 1000? '0px' : '20px' }}>	
									Bugs? Problems?
								</Typography>	
								<Typography style={{ textAlign: window.innerWidth > 1000 ? 'right' : 'left' }}>	
									Contact Me: <a href="mailto:schochal@student.ethz.ch" style={{ color: 'white' }}>schochal@student.ethz.ch</a><br/>
								</Typography>	
								<Typography variant="h5" style={{ textAlign: window.innerWidth > 1000 ? 'right' : 'left', marginTop: '20px' }}>	
									Open Source
								</Typography>	
								<Typography style={{ textAlign: window.innerWidth > 1000 ? 'right' : 'left' }}>	
									The Source code of this application can be found on <a href="https://gitlab.ethz.ch/schochal/ddr" style={{ color: 'white' }}>https://gitlab.ethz.ch/schochal/ddr</a> and is distributed under the GNU AGPL 3.0.
								</Typography>	
							</Grid>	
						</Grid>
					</Container>
				</Box>
			</ThemeProvider>
    )
  }
}

ReactDOM.render(<Footer/>, document.getElementById('footer'));
