import React from 'react';
import ReactDOM from 'react-dom';

import {
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Alert,
  Box,
  Button,
  Chip,
  Divider,
  FormControlLabel,
  Snackbar,
  Switch,
  TextField,
  Typography,
} from '@mui/material';

import {Masonry} from '@mui/lab';

import { ThemeProvider } from '@mui/material/styles';

import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import DownloadIcon from '@mui/icons-material/Download';
import UploadIcon from '@mui/icons-material/Upload';
import EditIcon from '@mui/icons-material/Edit';

import theme from './theme';

import { 
  Container
} from '@mui/material';

import axios from 'axios';

class Team extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      team: props.team,
      comment: '',
      isAccepted: false,
      mark: '',
      file: '',
    }

    this.showMarkBadge = this.showMarkBadge.bind(this);
    this.getNames = this.getNames.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.getData = props.getData.bind(this);

    this.snackbarSuccess = props.snackbarSuccess.bind(this);
    this.snackbarError = props.snackbarError.bind(this);
  }

  getNames(users) {
    let names = '';
    users.map(user => {  
      names = names + ', ' + user.firstName + ' ' + user.lastName;
    });
    return names.substring(2, names.length);
  }

  showMarkBadge(reports) {
    console.log(this.state.team.id);
    let mark = -1;
    let isAccepted = false;
    reports.map((report, id) => {
      if(report.isByAdmin && mark == -1) {
        mark = report.mark;
        isAccepted = report.isAccepted;
      }
    });
    if(mark == -1) {
      return (
        <></>
      );
    } else {
      return (
        <Chip label={mark} color={isAccepted ? "success" : "error"}/>
      );
    }
  }

  handleSubmit(e) {
    e.preventDefault();
    if(this.state.file != '') {
      let formData = new FormData();
      formData.append('file', this.state.file);
      axios.post('/reports/reports-api-file', formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      }).then((response) => {
        axios.put('/reports/reports-api-insert', {
          teamId: e.target.value,
          isAccepted: this.state.isAccepted,
          mark: this.state.mark,
          comment: this.state.comment,
          file: this.state.file.name,
        }).then(response => {
          this.setState({
            file: '',
            comment: '',
            mark: '',
            isAccepted: false,
          });
          this.getData();
          this.snackbarSuccess();
        });
      });
    } else {
      this.snackbarError();
    }
  }

  render() {
    return (
      <Accordion>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Typography variant="h6">
            Team {this.state.team.teamNr}: {this.getNames(this.state.team.users)} {this.state.team.reports.length != 0 && (!this.state.team.reports[0].isByAdmin && <EditIcon/>)} {this.props.showMarks && this.showMarkBadge(this.state.team.reports)}
          </Typography>
        </AccordionSummary>  
        <AccordionDetails>
          <Box>
            {this.state.team.reports.map((report, index) => (
              <Accordion key={index}>
                <AccordionSummary
                  expandIcon={<ExpandMoreIcon />}
                  aria-controls="panel1a-content"
                  id="panel1a-header"
                >
                  <Typography variant="h6" style={{ marginRight: '10px' }}>{report.timestamp} ({report.isByAdmin ? "Corrected" : "Hand-In"})</Typography>{(report.isByAdmin) && (report.isAccepted ?<Chip label="accpted" color="success" /> : <Chip label="not accepted" color="error"/> )}{(report.isByAdmin) && <Chip color="primary" label={report.mark} style={{ marginLeft: '10px' }}/>}
                </AccordionSummary>
                <AccordionDetails>
                  <Typography style={{ whiteSpace: 'pre-wrap' }}>{report.comment}</Typography>
                  {report.isByAdmin &&
                  <Box>
                    <Divider style={{ marginTop: '10px', marginBottom: '10px' }} />
                    <Typography>
                      Status: {report.isAccepted ? "accepted" : "not accepted"}
                    </Typography>
                    <Typography>
                      Mark: {report.mark}
                    </Typography> 
                  </Box> }
                  <Button
                    style={{ marginTop: '10px' }}
                    variant="contained"
                    startIcon={<DownloadIcon/>}
                    onClick={() => window.open('/reports/' + report.filename)}  
                  >
                    Download
                  </Button>
                </AccordionDetails>
              </Accordion>
            ))}
            <TextField 
              style={{ marginTop: '20px', marginBottom: '10px' }}
              id="outlined-basic" 
              label="Comments" 
              variant="outlined" 
              value={this.state.current == this.state.team.id ? this.state.comment : ''}
              onChange={(e) => this.setState({current: this.state.team.id, comment: e.target.value})}
              multiline
              fullWidth
            />
            <TextField 
              style={{ marginBottom: '10px' }}
              id="outlined-basic" 
              label="Mark" 
              variant="outlined" 
              value={this.state.current == this.state.team.id ? this.state.mark : ''}
              onChange={(e) => this.setState({current: this.state.team.id, mark: e.target.value})}
              fullWidth
            />
            <FormControlLabel control=
              <Switch
                checked={this.state.isAccepted}
                onChange={() => this.setState({isAccepted: !this.state.isAccepted})}
                inputProps={{ 'aria-label': 'controlled' }}
              />
              label={this.state.isAccepted ? "Accepted" : "Not Accepted"}
              style={{ marginBottom: '20px' }}
            /><br/>
            <Button
              style={{ marginBottom: '10px' }}
              variant="contained"
              component="label"
              startIcon={<UploadIcon/>}
            >
              <input name="upload-file" type="file" accept="application/pdf" hidden onChange={(e) => this.setState({file: e.target.files[0]})}/>
              Upload Corrected Report
            </Button>
            {this.state.file != '' && 
              <Typography component="span">  Selected: {this.state.file.name}</Typography>
            } 
            <br/>
            <Button
              type="submit"
              style={{ marginBottom: '10px' }}
              variant="contained"
              value={this.state.team.id}
              onClick={(e) => this.handleSubmit(e)}  
            >
              Submit
            </Button>
          </Box>
        </AccordionDetails>
      </Accordion>
    );
  }
}

class AdminReports extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      groups: [],
      showMarks: false,
      snackbar: false,
      snackbarError: false,
    }
    
    this.getData = this.getData.bind(this);
    this.getMarks = this.getMarks.bind(this);

    this.snackbarError = this.snackbarError.bind(this);
    this.snackbarSuccess = this.snackbarSuccess.bind(this);
  }

  componentDidMount() {
    this.getData();
  }

  snackbarError() {
    this.setState({snackbarError: true});
  }

  snackbarSuccess() {
    this.setState({
      snackbar: true,
      snackbarError: false,
    });
  }

  async getData() {
    const response = await fetch('/admin/admin-api');
    const data = await response.json();
    this.setState({
      groups: data,
    });
  }

  
  getMarks() {
    let contents = 'group,team,nethz,first name,last name,grade,comment\n';
    this.state.groups.map(group => {
      group.teams.map(team => {
        let mark = -1;
        let isAccepted = false;
        team.reports.map((report, id) => {
          if(report.isByAdmin && mark == -1) {
            mark = report.mark;
            isAccepted = report.isAccepted;
          }
        });
        team.users.map(user => {
          contents += group.GroupNr + ',' + team.teamNr + ',' + user.username + ',' + user.firstName + ',' + user.lastName + ',' + (mark == -1 ? '' : mark) + ',' + (isAccepted ? '' : 'not yet accepted') + '\n';
        });
      });
      contents += '\n';
    });

    let element = document.createElement('a');
    element.setAttribute('href', 'data:text/csv;charset=utf-8,' + encodeURIComponent(contents));
    element.setAttribute('download', 'SDG_Grades.csv');

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
  }

  render () { 
    return (
      <Container maxWidth="xl">
        <ThemeProvider theme={theme}>
          <Typography variant="h4" style={{ marginTop: '20px' }}>Reports</Typography>
          <FormControlLabel control=
            <Switch
              checked={this.state.showMarks}
              onChange={() => this.setState({showMarks: !this.state.showMarks})}
              inputProps={{ 'aria-label': 'controlled' }}
            />
            label={this.state.showMarks ? "show Marks" : "Don't show marks"}
            style={{ marginBottom: '20px', marginTop: '20px' }}
          /><br/>
          <Button
            variant="outlined"
            onClick={this.getMarks}
          >
            Download Grade List
          </Button>
          {this.state.groups.map(group => (
            <Box key={group.id} style={{ marginTop: '20px' }}>
              <Typography variant="h5" style={{ marginBottom: '10px' }}>
                Group {group.GroupNr}
              </Typography>
              {group.teams.map(team => (
                <Team team={team} showMarks={this.state.showMarks} getData={this.getData} snackbarSuccess={this.snackbarSuccess} snackbarError={this.snackbarError} key={team.id + '_' + team.reports.length}/>
              ))}
            </Box>
          ))}
          <Snackbar open={this.state.snackbar} autoHideDuration={6000} onClose={() => {this.setState({snackbar: false});}}>
            <Alert severity="success" sx={{ width: '100%' }}>
             The Report was successfully added! 
            </Alert>
          </Snackbar>
          <Snackbar open={this.state.snackbarError} autoHideDuration={6000} onClose={() => {this.setState({snackbarError: false});}}>
            <Alert severity="error" sx={{ width: '100%' }}>
             Please completely fill out the form
            </Alert>
          </Snackbar>
        </ThemeProvider>
      </Container>
   )
  }
}

ReactDOM.render(<AdminReports/>, document.getElementById('admin_reports'));
