import React from 'react';
import ReactDOM from 'react-dom';

import {
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Alert,
  Autocomplete,
  Button,
  Grid,
  Snackbar,
  TextField,
  Typography,
} from '@mui/material';

import LoadingButton from '@mui/lab/LoadingButton';

import { ThemeProvider } from '@mui/material/styles';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import FileUploadIcon from '@mui/icons-material/FileUpload';

import theme from './theme';

import axios from 'axios';

class ReportForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
			file: '',
			comment: '',
      snackbar: false,
      snackbarError: false,
			isUploading: false,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(e) {
		e.preventDefault();
    if(this.state.file != '') {
			this.setState({isUploading: true});
      let formData = new FormData();
      formData.append('file', this.state.file);
      axios.post('/reports/reports-api-file', formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      }).then((response) => {
        axios.put('/reports/reports-api-insert', {
          comment: this.state.comment,
          file: this.state.file.name,
        }).then(response => {
          this.setState({
            file: '',
            comment: '',
            snackbar: true,
            snackbarError: false,
						isUploading: false,
          });
        });
				this.props.getData();
      });
    } else {
      this.setState({snackbarError: true});
    }
  }

  render() {
    return (
      <ThemeProvider theme={theme}>
        <Accordion style={{ marginTop: '20px' }}>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon/>}
            aria-controls="panel1a-content"
          >
            <Typography variant="h5">
              Upload Report
            </Typography>
          </AccordionSummary>
          <AccordionDetails>
						<form>
							<Grid container spacing={2}>
								<Grid item xs={12}>
									<TextField 
										fullWidth
										label="Comment" 
										variant="outlined" 
										value={this.state.comment}
										multiline
										onChange={(e) => this.setState({comment: e.target.value})}
									/>
								</Grid>
								<Grid item xs={12}>
									<Button
										variant="contained"
										component="label"
										startIcon={<FileUploadIcon/>}
									>
										<input name="upload-photo" type="file" accept="application/pdf" hidden onChange={(e) => this.setState({file: e.target.files[0]})}/>
										Upload Report
									</Button>
									{this.state.file != '' && 
										<Typography component="span">  Selected: {this.state.file.name}</Typography>
									}
								</Grid>
								<Grid item xs={12}>
									{this.state.isUploading ? 
									<LoadingButton
										variant="contained"
										loading
									>
										Submit
									</LoadingButton> :
									<Button
										type="submit"
										variant="contained"
										onClick={(e) => this.handleSubmit(e)}
									>
										Submit
									</Button>
									}
								</Grid>
							</Grid>
						</form>
          </AccordionDetails>
        </Accordion>
        <Snackbar open={this.state.snackbar} autoHideDuration={6000} onClose={() => {this.setState({snackbar: false});}}>
          <Alert severity="success" sx={{ width: '100%' }}>
           The Report was successfully added! 
          </Alert>
        </Snackbar>
        <Snackbar open={this.state.snackbarError} autoHideDuration={6000} onClose={() => {this.setState({snackbarError: false});}}>
          <Alert severity="error" sx={{ width: '100%' }}>
           Please completely fill out the form
          </Alert>
        </Snackbar>
      </ThemeProvider>
    )
  }
}

export default ReportForm;
