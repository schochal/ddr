import React from 'react';
import ReactDOM from 'react-dom';

import {
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Box,
  Button,
  Card,
  CardContent,
  Chip,
  Divider,
  Grid,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Paper,
  Typography,
  Tabs,
  Tab,
} from '@mui/material';

import {Masonry} from '@mui/lab';

import GroupsIcon from '@mui/icons-material/Groups';
import GroupIcon from '@mui/icons-material/Group';
import PersonIcon from '@mui/icons-material/Person';
import ScienceIcon from '@mui/icons-material/Science';

import { ThemeProvider } from '@mui/material/styles';

import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import DownloadIcon from '@mui/icons-material/Download';
import CheckIcon from '@mui/icons-material/Check';
import ClearIcon from '@mui/icons-material/Clear';
import OpenInNewIcon from '@mui/icons-material/OpenInNew';
import SsidChartIcon from '@mui/icons-material/SsidChart';
import ScatterPlotIcon from '@mui/icons-material/ScatterPlot';

import theme from './theme';

import { 
  Container
} from '@mui/material';

import ReportsForm from './reportsForm';

class Reports extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      user: null,
      reports: [],
      TREVAC: [],
    }

    this.getData = this.getData.bind(this);
    this.getUser = this.getUser.bind(this);
    this.compoundslink = this.compoundslink.bind(this);
    this.getNames = this.getNames.bind(this);
    this.findTREVACFiles = this.findTREVACFiles.bind(this);
    this.downloadTREVAC = this.downloadTREVAC.bind(this);
  }

  componentDidMount() {
    this.getUser();
    this.getData();
    this.findTREVACFiles();
  }

  async getData() {
    const response = await fetch('/reports/reports-api');
    const data = await response.json();
    this.setState({
      reports: data,
    });
  }

  async getUser() {
    const response = await fetch('/user-api');
    const data = await response.json();
    this.setState({
      user: data,
    });
  }

  async findTREVACFiles() {
    const response = await fetch('/reports/reports-api-data');
    const data = await response.json();
    this.setState({
      TREVAC: data,
    });
  }

  downloadTREVAC() {
    var link = document.createElement('a');

    link.setAttribute('download', null);
    link.style.display = 'none';

    document.body.appendChild(link);

    this.state.TREVAC.map((file) => {
      link.setAttribute('href', '/TREVAC/' + file);
      link.setAttribute('download', file);
      link.click();
    });

    document.body.removeChild(link);
  }

  compoundslink(team) {
    if(team != null) {
      return '/literature/lit_' + team.groupNr.groupNr + '_' + team.teamNr + '.pdf';
    } else
      return ' ';
  }

  getNames(users) {
    let names = '';
    users.map(user => {  
      names = names + ', ' + user.firstName + ' ' + user.lastName;
    });
    return names.substring(2, names.length);
  }

  render () { 
    return (
      <Container maxWidth="xl">
        <ReportsForm getData={this.getData}/>

        <ThemeProvider theme={theme}>
          <Typography variant="h4" style={{ marginTop: '20px' }}>Reports</Typography>

          {this.state.user != null && 
          <List>
            <ListItem disablePadding>
              <ListItemIcon>
                <GroupsIcon />
              </ListItemIcon>
              <ListItemText primary={"Group " + this.state.user.team.groupNr.groupNr} />
            </ListItem>
            <ListItem disablePadding>
              <ListItemIcon>
                <GroupIcon />
              </ListItemIcon>
              <ListItemText primary={"Team " + this.state.user.team.teamNr} />
            </ListItem>
            <ListItem disablePadding>
              <ListItemIcon>
                <ScienceIcon />
              </ListItemIcon>
              <ListItemText primary={this.state.user.team.compounds} />
            </ListItem>
          </List>}
  
          <Box style={{ marginTop: '20px' }}>
            {this.state.reports.map((report, index) => (
              <Accordion key={index}>
                <AccordionSummary
                  expandIcon={<ExpandMoreIcon />}
                  aria-controls="panel1a-content"
                  id="panel1a-header"
                >
                  <Typography variant="h6" style={{ marginRight: '10px' }}>{report.timestamp} ({report.isByAdmin ? "Corrected" : "Hand-In"})</Typography>{(report.isByAdmin) && (report.isAccepted ?<Chip label="accpted" color="success" /> : <Chip label="not accepted" color="error"/> )}{(report.isByAdmin) && <Chip color="primary" label={report.mark} style={{ marginLeft: '10px' }}/>}
                </AccordionSummary>
                <AccordionDetails>
                  <Typography style={{ whiteSpace: 'pre-wrap' }}>{report.comment}</Typography>
                  {report.isByAdmin &&
                  <Box>
                    <Divider style={{ marginTop: '10px', marginBottom: '10px' }} />
                    <Typography>
                      Status: {report.isAccepted ? "accepted" : "not accepted"}
                    </Typography>
                    <Typography>
                      Mark: {report.mark}
                    </Typography> 
                  </Box> }
                  <Button
                    component="a"
                    variant="contained"
                    href={'/reports/' + report.filename}  
                    style={{ marginTop: '10px', color: 'white' }}
                    startIcon={<DownloadIcon/>}
                  >
                    Download
                  </Button>
                </AccordionDetails>
              </Accordion>
            ))}
           </Box>
          {this.state.user !== null &&
          <>
          <Button
            component="a"
            style={{ marginTop: '10px', marginRight: '10px', color: 'white' }}
            variant="contained"
            startIcon={<OpenInNewIcon/>}
            href='https://gitlab.com/alexander_schoch/acpc1-template'
          >
            LaTeX-Template
          </Button>
          <Button
            component="a"
            style={{ marginTop: '10px', color: 'white', marginRight: '10px' }}
            variant="contained"
            startIcon={<SsidChartIcon/>}
            href={this.compoundslink(this.state.user.team)}
          >
            Literature Plots
          </Button>
          {this.state.TREVAC.length > 0 && 
          <Button
            style={{ marginTop: '10px', color: 'white' }}
            variant="contained"
            startIcon={<ScatterPlotIcon/>}
            onClick={() => {this.downloadTREVAC()}}
          >
            TREVAC Data
          </Button>}
          </>}
        </ThemeProvider>
      </Container>
    )
  }
}

ReactDOM.render(<Reports/>, document.getElementById('reports'));
