import React from 'react';
import ReactDOM from 'react-dom';

import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import ListItemIcon from '@mui/material/ListItemIcon';
import Drawer from '@mui/material/Drawer';
import Container from '@mui/material/Container';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import Tooltip from '@mui/material/Tooltip';
import MenuItem from '@mui/material/MenuItem';

import AdminPanelSettingsIcon from '@mui/icons-material/AdminPanelSettings';
import ArticleIcon from '@mui/icons-material/Article';
import AutoGraphIcon from '@mui/icons-material/AutoGraph';
import HomeIcon from '@mui/icons-material/Home';
import LoginIcon from '@mui/icons-material/Login';
import LogoutIcon from '@mui/icons-material/Logout';
import MenuIcon from '@mui/icons-material/Menu';
import PasswordIcon from '@mui/icons-material/Password';
import ScatterPlotIcon from '@mui/icons-material/ScatterPlot';

import {ThemeProvider} from '@mui/material/styles';
import { styled } from '@mui/material/styles';
import theme from './theme';

class Navbar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      drawer: false,
			user: {},
    };

		this.isLoggedIn = this.isLoggedIn.bind(this);
		this.getUser = this.getUser.bind(this);
    this.handleOpenNavMenu = this.handleOpenNavMenu.bind(this);
    this.handleCloseNavMenu = this.handleCloseNavMenu.bind(this);
  }

	componentDidMount() {
		this.getUser();
	}

	isLoggedIn() {
		if(Object.entries(this.state.user).length == 0)
			return false;
		return true;
	}

	async getUser() {
    const response = await fetch('/user-api');
    const data = await response.json();
    this.setState({
      user: data,
    });
  }

  handleOpenNavMenu = () => {
    this.setState({drawer: true});
  };

  handleCloseNavMenu = () => {
    this.setState({drawer: false});
  };

  render () {
    return (
      <Box>
      <ThemeProvider theme={theme}>
      <AppBar position="sticky" style={{ backgroundColor: theme.palette.primary.dark }} elevation={0}>
        <Container maxWidth="xl">
          <Toolbar disableGutters>
            <Box sx={{ flexGrow: 1, display: { xs: 'flex', lg: 'none' } }}>
              <IconButton
                size="large"
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={this.handleOpenNavMenu}
                color="inherit"
              >
                <MenuIcon />
              </IconButton>
              <Drawer
                anchor='left'
                open={this.state.drawer}
                onClose={this.handleCloseNavMenu}
              >
                <List>
									{this.isLoggedIn() &&
									<ListItem>
										<Typography>
											Logged in as {this.state.user.firstName} {this.state.user.lastName}
										</Typography>
									</ListItem>
									}
									<ListItem button>
										<ListItemIcon>
											<HomeIcon/>
										</ListItemIcon>
										<ListItemText primary="Home" onClick={() => {window.location = '/';}} />
									</ListItem> 
									<ListItem button>
										<ListItemIcon>
											<ScatterPlotIcon/>
										</ListItemIcon>
										<ListItemText primary="Data Evaluation" onClick={() => {window.location = '/data_evaluation';}} />
									</ListItem> 
									<ListItem button>
										<ListItemIcon>
											<AutoGraphIcon/>
										</ListItemIcon>
										<ListItemText primary="Clausius-Clapeyron" onClick={() => {window.location = '/clausius-clapeyron';}} />
									</ListItem> 
									{ !this.isLoggedIn() ?
									<ListItem button>
										<ListItemIcon>
											<LoginIcon/>
										</ListItemIcon>
										<ListItemText primary="Login" onClick={() => {window.location = '/login';}} />
									</ListItem> :
									<>
									<ListItem button>
										<ListItemIcon>
											<ArticleIcon/>
										</ListItemIcon>
										<ListItemText primary="Reports" onClick={() => {window.location = this.state.user.roles.includes("ROLE_ADMIN") ? '/admin/reports' : '/reports';}} />
									</ListItem> 
									{ this.state.user.roles.includes("ROLE_ADMIN") &&
										<ListItem button>
											<ListItemIcon>
												<AdminPanelSettingsIcon/>
											</ListItemIcon>
											<ListItemText primary="Admin" onClick={() => {window.location = '/admin';}} />
										</ListItem> 
									}
									<ListItem button>
										<ListItemIcon>
											<PasswordIcon/>
										</ListItemIcon>
										<ListItemText primary="Change Password" onClick={() => {window.location = '/change_password';}} />
									</ListItem>
									<ListItem button>
										<ListItemIcon>
											<LogoutIcon/>
										</ListItemIcon>
										<ListItemText primary="Logout" onClick={() => {window.location = '/logout';}} />
									</ListItem>
									</>
									}
                </List>
              </Drawer>
            </Box>
            { 
              // Desktop 
            }
            <a href="/">
              <img src="/logo.svg" style={{ maxWidth: '150px' }}/>
            </a>
            <Box sx={{ flexGrow: 1, display: { xs: 'none', lg: 'flex' }, marginLeft: '30px' }}>
								<Button
									component="a"
									href="/"
									sx={{ my: 2, color: 'white', display: 'block', "&:hover": {color: 'white'} }}
								>
									Home
								</Button> 
								<Button
									component="a"
									href="/data_evaluation"
									sx={{ my: 2, color: 'white', display: 'block', "&:hover": {color: 'white'} }}
								>
									Data Evaluation
								</Button> 
								<Button
									component="a"
									href="/clausius-clapeyron"
									sx={{ my: 2, color: 'white', display: 'block', "&:hover": {color: 'white'} }}
								>
									Clausius-Clapeyron
								</Button> 
							{ !this.isLoggedIn() ?
								<Button
									component="a"
									href="/login"
									sx={{ my: 2, color: 'white', display: 'block', "&:hover": {color: 'white'} }}
								>
									Login
								</Button> 
								:
								<>
								<Button
									component="a"
									href={this.state.user.roles.includes("ROLE_ADMIN") ? '/admin/reports' : '/reports'}
									sx={{ my: 2, color: 'white', display: 'block', "&:hover": {color: 'white'} }}
								>
									Reports
								</Button> 
								{ this.state.user.roles.includes("ROLE_ADMIN") &&
								<Button
									component="a"
									href="/admin"
									sx={{ my: 2, color: 'white', display: 'block', "&:hover": {color: 'white'} }}
								>
									Admin
								</Button> }
								<Button
									component="a"
									href="/change_password"
									sx={{ my: 2, color: 'white', display: 'block', "&:hover": {color: 'white'} }}
								>
									Change Password
								</Button>
								<Button
									component="a"
									href="/logout"
									sx={{ my: 2, color: 'white', display: 'block', "&:hover": {color: 'white'} }}
								>
									Logout
								</Button>
								</>
							}
            </Box>
						{ window.innerWidth > 1025 && this.isLoggedIn() &&
							<Typography component="div" style={{ display: 'block' }}>
								Logged in as {this.state.user.firstName} {this.state.user.lastName}
							</Typography>
						}
          </Toolbar>
        </Container>
      </AppBar>
      </ThemeProvider>
      </Box>
    );
  }
}

ReactDOM.render(<Navbar/>, document.getElementById('navbar'));
