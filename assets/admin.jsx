import React from 'react';
import ReactDOM from 'react-dom';

import {
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Alert,
  Autocomplete,
  Box,
  Button,
  Container,
  Grid,
  List,
  ListItem,
  ListItemText,
  Paper,
  Snackbar,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
  Typography,
} from '@mui/material';

import LoadingButton from '@mui/lab/LoadingButton';

import { ThemeProvider } from '@mui/material/styles';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import FileUploadIcon from '@mui/icons-material/FileUpload';

import theme from './theme';

import axios from 'axios';

class Admin extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
			groups: [],
			file: '',
      snackbar: false,
			copied: false,
      snackbarError: false,
			isUploading: false,
    };
		this.getData = this.getData.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
		this.getNames = this.getNames.bind(this);
		this.getNethz = this.getNethz.bind(this);
		this.compoundslink = this.compoundslink.bind(this);
  }

  async getData() {
    const response = await fetch('/admin/admin-api');
    const data = await response.json();
    this.setState({
      groups: data,
    });
  }

	componentDidMount() {
		this.getData();
	}
	
  handleSubmit(e) {
		e.preventDefault();
    if(this.state.file != '') {
			this.setState({isUploading: true});
      let formData = new FormData();
      formData.append('file', this.state.file);
      axios.post('/admin/admin-api-students', formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      }).then(response => {
				this.setState({
					file: '',
					snackbar: true,
					copied: false,
					snackbarError: false,
					isUploading: false,
				});
			});
    } else {
      this.setState({snackbarError: true});
    }
  }

	getNethz(users) {
		let nethz = '';
		users.map(user => {	
			nethz = nethz + ', ' + user.username;
		});
		return nethz.substring(2, nethz.length);
	}

	getNames(users) {
		let names = '';
		users.map(user => {	
			names = names + ', ' + user.firstName + ' ' + user.lastName;
		});
		return names.substring(2, names.length);
	}

	copyEmails(e) {
		this.setState({copied: true});
		let emails = '';
		this.state.groups[e].teams.map((team) => {
			team.users.map((user) => {
				emails = emails + ', ' + user.username + '@student.ethz.ch';
			})
		});
		navigator.clipboard.writeText(emails.substring(2, emails.length));
		return;
	}

	compoundslink(compounds) {
		if(compounds != null) {
			let a = compounds.split('/');
			let first = a[0].split('(')[0].replace(" ", "-");
			let second = a[1].split('(')[0].substr(1).replace(" ", "-");
			return '/literature/lit_' + first.substr(0, first.length-1) + '_' + second.substr(0, second.length-1) + '.pdf';
		} else
			return ' ';
	}

  render() {
    return (
      <ThemeProvider theme={theme}>
				<Container maxWidth="xl">
					<Typography variant="h4" style={{ marginTop: '20px' }}>Admin Panel</Typography>
					<Accordion style={{ marginTop: '20px' }} elevation={6}>
						<AccordionSummary
							expandIcon={<ExpandMoreIcon/>}
							aria-controls="panel1a-content"
						>
							<Typography variant="h5">
								Add Student List
							</Typography>
						</AccordionSummary>
						<AccordionDetails>
							<form>
								<Grid container spacing={2}>
									<Grid item xs={12}>
										<Typography>
											Upload a .csv file with the following columns (in this order, no header):
										</Typography>
										<ul>
											<li><Typography>nethz</Typography></li>
											<li><Typography>first name</Typography></li>
											<li><Typography>last name</Typography></li>
											<li><Typography>group number</Typography></li>
											<li><Typography>team number</Typography></li>
											<li><Typography>pronouns (m/f)</Typography></li>
											<li><Typography>compounds</Typography></li>
										</ul>
									</Grid>
									<Grid item xs={12}>
										<Button
											variant="contained"
											component="label"
											startIcon={<FileUploadIcon/>}
										>
											<input name="upload-photo" type="file" accept=".csv" hidden onChange={(e) => this.setState({file: e.target.files[0]})}/>
											Upload Student List
										</Button>
										{this.state.file != '' && 
											<Typography component="span">  Selected: {this.state.file.name}</Typography>
										}
									</Grid>
									<Grid item xs={12}>
										{this.state.isUploading ? 
										<LoadingButton
											variant="contained"
											loading
										>
											Submit
										</LoadingButton> :
										<Button
											type="submit"
											variant="contained"
											onClick={(e) => this.handleSubmit(e)}
										>
											Submit
										</Button>}
									</Grid>
								</Grid>
							</form>
						</AccordionDetails>
					</Accordion>

					{ /* Student List */ }
					{this.state.groups.map((group, id) => (
						<Box key={group.id} style={{ marginTop: '20px' }}>
							<Typography component="span" variant="h5" style={{ marginTop: '20px' }}>Group {group.GroupNr}</Typography>
							<Button 
								variant="outlined"
								value={id}
								onClick={(e) => this.copyEmails(e.target.value)}
								style={{ marginLeft: '10px' }}
							>
								Copy Email-Addresses
							</Button>
							<TableContainer component={Paper} style={{ marginTop: '10px' }}>
								<Table sx={{ minWidth: 650 }} aria-label="simple table">
									<TableHead>
										<TableRow>
											<TableCell><b>Team</b></TableCell>
											<TableCell><b>Students</b></TableCell>
											<TableCell><b>nethz</b></TableCell>
											<TableCell><b>Compounds</b></TableCell>
											<TableCell><b>Links</b></TableCell>
										</TableRow>
									</TableHead>
									<TableBody>
										{group.teams.map(team => (
											<TableRow key={team.id}>
												<TableCell><Typography>{team.teamNr}</Typography></TableCell>
												<TableCell><Typography>{this.getNames(team.users)}</Typography></TableCell>
												<TableCell><Typography>{this.getNethz(team.users)}</Typography></TableCell>
												<TableCell><Typography>{team.compounds}</Typography></TableCell>
												<TableCell><Typography><Button variant="outlined" component="a" style={{ marginRight: '5px', marginBottom: '5px' }} href={'/literature/lit_' + group.GroupNr + '_' + team.teamNr + '.pdf'}>Literature Plots</Button></Typography></TableCell>
											</TableRow>
										))}
									</TableBody>
								</Table>
							</TableContainer>
						</Box>
					))}

					<Snackbar open={this.state.snackbar} autoHideDuration={6000} onClose={() => {this.setState({snackbar: false});}}>
						<Alert severity="success" sx={{ width: '100%' }}>
						 The Students were successfully added! 
						</Alert>
					</Snackbar>
					<Snackbar open={this.state.copied} autoHideDuration={6000} onClose={() => {this.setState({copied: false});}}>
						<Alert severity="success" sx={{ width: '100%' }}>
							Copied!
						</Alert>
					</Snackbar>
					<Snackbar open={this.state.snackbarError} autoHideDuration={6000} onClose={() => {this.setState({snackbarError: false});}}>
						<Alert severity="error" sx={{ width: '100%' }}>
						 Please completely fill out the form
						</Alert>
					</Snackbar>
				</Container>
			</ThemeProvider>
    )
  }
}

ReactDOM.render(<Admin/>, document.getElementById('admin'));
